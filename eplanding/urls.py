"""eplanding URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import TemplateView
from landing import views

disclaimer = TemplateView.as_view(template_name='landing/disclaimer.html')

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^markdownx/', include('markdownx.urls')),
    url(r'^$', views.index_page, name='index'),
    url(r'^hashtag/$', views.hashtag_page, name='hashtag'),
    url(r'^getcred/$', views.get_cred, name='getcred'),
    url(r'^uploadtwitter/$', views.upload_to_twitter, name='twitterupload'),
    url(r'^deleteaccount/$', views.deleteaccount, name='deleteaccount'),
    url(r'^news/$', views.news_page, name='newses'),
    url(r'^news/(?P<id>\d+)/$', views.news_detail_page, name='news_detail'),
    url(r'^getimage/$', views.getimage, name='getimage'),
    url(r'^saveimage/$', views.savehashtag, name='saveimage'),
    url(r'^disclaimer/$', disclaimer, name='disclaimer'),
    url(r'^downloaded/$', views.downloadcount, name='downloaded'),
]
