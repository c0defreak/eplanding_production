from django.contrib import admin

from markdownx.admin import MarkdownxModelAdmin

from landing.models import News, Tags, Profile, HashtagImg, DownloadCounter


class CounterModelAdmin(admin.ModelAdmin):
    readonly_fields = ('downloaded',)

# Register your models here.
admin.site.register(News, MarkdownxModelAdmin)
admin.site.register(Tags)
admin.site.register(Profile)
admin.site.register(HashtagImg)
admin.site.register(DownloadCounter, CounterModelAdmin)
