import json
import os
import tempfile
import requests
import tweepy

from django.http import HttpResponse, HttpResponseForbidden
from django.shortcuts import render
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.gis.geoip2 import GeoIP2
from allauth.socialaccount.models import SocialToken, SocialApp
from utils import send_contact_email

from landing.models import News, Tags, HashtagImg, DownloadCounter


# Create your views here.


def index_page(request):
    if request.method == 'POST':
        name = request.POST.get('name', '')
        email = request.POST.get('email', '')
        subject = request.POST.get('subject', '')
        message = request.POST.get('message', '')
        send_contact_email(
            email=email,
            name=name,
            subject=subject,
            message=message
        )
    return render(
        request,
        'landing/index.html'
    )


def hashtag_page(request):
    hashtagimges = HashtagImg.objects.all()
    return render(
        request,
        'landing/hashtag.html',
        {
            'hashtagimges': hashtagimges
        }
    )


def getimage(request):
    id = request.GET.get('id')
    hashtag = HashtagImg.objects.get(id=id)
    data = json.dumps({
        'uploader': hashtag.uploader,
        'img': hashtag.image.url
    })
    content_type = 'application/json'
    return HttpResponse(data, content_type)


def get_cred(request):
    social_accounts = request.user.socialaccount_set.all()
    user_has_facebook = False
    response_dict = {}
    for social_account in social_accounts:
        if social_account.get_provider_display() == 'Facebook':
            user_has_facebook = True
            account = social_account
            break
    if user_has_facebook:
        response_dict['has_facebook'] = True
        tokens = SocialToken.objects.filter(
            account__user=request.user,
            account__provider='facebook'
        )
        response_dict['access_token'] = tokens[0].token
        response_dict['uid'] = account.uid
    else:
        response_dict['has_facebook'] = False
    data = json.dumps(response_dict)
    mimetype = 'application/json'
    return HttpResponse(data, mimetype)


def upload_to_twitter(request):
    if request.method == 'POST':
        source = request.FILES.get('source')
        twitter = SocialApp.objects.get(name='twitter')
        tokens = SocialToken.objects.filter(
            account__user=request.user,
            account__provider='twitter'
        )
        consumer_key = twitter.client_id
        consumer_secret = twitter.secret
        access_token = tokens[0].token
        access_token_secret = tokens[0].token_secret
        auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
        auth.set_access_token(access_token, access_token_secret)
        api = tweepy.API(auth)
        f = tempfile.NamedTemporaryFile(
            prefix='twitter',
            suffix='.png',
            delete=False
        )
        f.writelines(source)
        f.close()
        api.update_profile_image(f.name)
        os.remove(f.name)
        request.user.delete()
        return HttpResponse("Ok")
    else:
        return HttpResponseForbidden()


def deleteaccount(request):
    if request.user.username != 'mashiweirk':
        request.user.delete()
    return HttpResponse("Ok")


def news_page(request):
    newses = News.objects.filter(draft=False)
    trending = newses.order_by('-clickcount')
    if trending:
        trending = trending[:3]
    else:
        trending = None
    if 'tag' in request.GET:
        tag = request.GET.get('tag')
        tag = Tags.objects.get(name=tag)
        newses = newses.filter(tags=tag)
    if 'author' in request.GET:
        author = request.GET.get('author')
        author = User.objects.get(username=author)
        newses = newses.filter(author=author)
    return render(
        request,
        'landing/news.html',
        {
            'newses': newses,
            'trending': trending,
            'tags': Tags.objects.all()
        }
    )


def news_detail_page(request, id):
    news = News.objects.get(id=id)
    news.clickcount += 1
    news.save()
    return render(
        request,
        'landing/newsdetail.html',
        {
            'news': news
        }
    )


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def savehashtag(request):
    if request.method == 'POST':
        g = GeoIP2()
        latlng = g.lat_lon(get_client_ip(request))
        hashtagimg = HashtagImg()
        hashtagimg.lat = latlng[0]
        hashtagimg.lng = latlng[1]
        hashtagimg.uploader = request.POST.get('uploader')
        hashtagimg.image = request.FILES.get('source')
        hashtagimg.save()
    return HttpResponse("OK")


def downloadcount(self):
    d, created = DownloadCounter.objects.get_or_create(id=1)
    d.downloaded += 1
    d.save()
    return HttpResponse()
