from __future__ import unicode_literals

from django.db import models
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from markdownx.models import MarkdownxField
from versatileimagefield.fields import VersatileImageField
from versatileimagefield.placeholder import OnStoragePlaceholderImage

# Create your models here.


class Profile(models.Model):
    user = models.OneToOneField(User, related_name='profile')
    bio = models.TextField(default='')
    avatar = VersatileImageField(
        upload_to='avatar',
        null=True,
        blank=True,
        placeholder_image=OnStoragePlaceholderImage(
            path='default.png'
        )
    )

    def __unicode__(self):
        return self.user.username


class Tags(models.Model):
    name = models.CharField(max_length=40)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Tags'


class News(models.Model):
    author = models.ForeignKey(User, related_name='news')
    tags = models.ManyToManyField(Tags, related_name='news')
    title = models.CharField(max_length=100)
    content = MarkdownxField()
    draft = models.BooleanField(default=True)
    created_on = models.DateTimeField(auto_now_add=True)
    clickcount = models.IntegerField(editable=False, default=0)

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('news_detail', args=[self.id, ])

    class Meta:
        verbose_name_plural = 'News'


class HashtagImg(models.Model):
    lat = models.CharField(max_length=40, default='')
    lng = models.CharField(max_length=40, default='')
    uploader = models.CharField(max_length=40, default='')
    image = VersatileImageField(
        upload_to='hashtagimg',
        blank=True,
        null=True
    )

    def __unicode__(self):
        return self.uploader


class DownloadCounter(models.Model):
    downloaded = models.IntegerField(editable=False)

    def __unicode__(self):
        return "Download Count"
