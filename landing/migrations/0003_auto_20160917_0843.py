# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-09-17 08:43
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('landing', '0002_auto_20160917_0841'),
    ]

    operations = [
        migrations.AlterField(
            model_name='news',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
